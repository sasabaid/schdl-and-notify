const mongoose = require('mongoose');

const AppStatusSchema = mongoose.Schema({
  appName: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('AppStatus', AppStatusSchema);
