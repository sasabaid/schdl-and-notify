require('dotenv/config');
const express = require('express');
const expressSession = require('express-session');
const logger = require('./helpers/logger');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const correlator = require('express-correlation-id');
const ping = require('./routes/ping');
const appStatusRoutes = require('./routes/appStatus');
const mongoose = require('mongoose');
const cron = require("node-cron");
const callFetchApi = require('./services/api');
const sendWhatsAppMsg = require('./whatsappNotify');
const MessagingResponse = require('twilio').twiml.MessagingResponse;

const PORT = process.env.PORT || 8000;

const app = express();
app.use(
    expressSession({
        secret: 'keyboard cat',
        resave: false,
        saveUninitialized: false
    })
);

app.use(logger.winston);
app.use(logger.express);
app.use(cookieParser());
app.use(correlator());
const corrMiddleware = function (req, res, next) {
    req.headers['X-Correlation-ID'] = req.correlationId();
    next();
};

app.use(corrMiddleware);
app.use((req, res, next) => {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '-1');
    res.header('Pragma', 'no-cache');
    next();
});

app.use(bodyParser.json({
    limit: '256kb'
}));
app.use(
    bodyParser.urlencoded({
        extended: true,
        limit: '256kb'
    })
);

app.use('/api/ping', ping);

app.post('/message', (req, res) => {
    body = 'Thank you for waking me up'
    const twiml = new MessagingResponse();
    twiml.message(body);
    res.writeHead(200, {'Content-Type': 'text/xml'});
    res.end(twiml.toString());
})

app.get('/api/from_orders', (req, res) => {
    sendWhatsAppNotificationForFromOrders();
    res.send('from_orders done');
})

app.get('/api/to_orders', (req, res) => {
    sendWhatsAppNotificationForToOrders();
    res.send('to_orders done');
})

app.get('/api/closed_orders', (req, res) => {
    sendWhatsAppNotificationForClosedOrders();
    res.send('closed_orders done');
})

app.get('/api/goods_reminder', (req, res) => {
    sendWhatsAppNotificationForGoodsReminder();
    res.send('goods_reminder done');
})

app.use('/appstatus', appStatusRoutes);

// 0 30 9,14,18 * * * (IST)
cron.schedule("0 0 4,9,13 * * *", function () {
    sendWhatsAppNotificationForFromOrders();
});

// 2 0 12,16 * * * (IST)
cron.schedule("2 30 6,10 * * *", function () {
    sendWhatsAppNotificationForToOrders();
});

// 0 0 23 * * * (IST)
cron.schedule("0 30 17 * * *", function () {
    sendWhatsAppNotificationForClosedOrders();
});

// 0 0 9,13,17 * * 0,6 (IST)
cron.schedule("0 30 3,7,11 * * 0,6", function () {
    sendWhatsAppNotificationForGoodsReminder();
});

// 12 0,15,30,45 8-23 * * * (IST)
cron.schedule("12 0,15,30,45 2-17 * * *", function () {
    callFetchApi(`${process.env.GH_ODRS_API_URL}/api/ping`, {}, 'GET')
    .then(response => console.log('response ======= GH_ODRS_API_URL=ping=========', response.data, response.status))
    .catch(err => console.log(err))
});

// 12 0,15,30,45 8-23 * * * (IST)
cron.schedule("42 0,15,30,45 2-17 * * *", function () {
    callFetchApi(`${process.env.FAMILY_TREE_API_URL}/api/ping`, {}, 'GET')
    .then(response => console.log('response ======= FAMILY_TREE_API_URL=ping=========', response.data, response.status))
    .catch(err => console.log(err))
});

mongoose.connect(process.env.DB_CONNECTION_URL, {
        useUnifiedTopology: true
    }, () =>
    console.log('Connected to DB!')
);

app.listen(PORT, error => {
    if (error) {
        console.error(error);
    }
    console.info(
        `==> 🌎 App Listening on ${PORT} please open your browser and navigate to http://localhost:${PORT}/`
    );
});

function sendWhatsAppNotificationForGoodsReminder() {
    let body = 'Hello, \nGood Morning.\nI hope your sales at the shop is going well. Please let us know if you need any of our following mens accessories.\n*_1. Ties_*\n*_2. Belt_*\n*_3. Socks_*\n*_4. Bow_*\n*_5. Wallet_*\n*_6. Cufflink Set_*\n*_7. Tie Pin_*\n*_8. Brouche_*\n\n_Regards_\n*GARMENT HOUSE*\n';
    sendWhatsAppMsg(process.env.TO_SUCCESS_PHONE_NUMBERS, body);
}

function sendWhatsAppNotificationForClosedOrders() {
    callFetchApi(`${process.env.GH_ODRS_API_URL}/orders/closed`, {}, 'GET')
        .then(response => {
            console.log('response ======= =closed =========', response.data, response.status);
            let body = '';
            let closedOrdersName = response.data;
            if (closedOrdersName.length === 0) {
                body = 'No Orders Closed today. Please Close karo orders agar close ho gaya koi';
            }
            else {
                closedOrdersName.forEach((name, index) => {
                    body += `${index + 1}. ${name}\n`;
                });
            }
            sendWhatsAppMsg(process.env.TO_SUCCESS_PHONE_NUMBERS, body);
        })
        .catch(err => console.log(err));
}

function sendWhatsAppNotificationForToOrders() {
    callFetchApi(`${process.env.GH_ODRS_API_URL}/orders/to_open`, {}, 'GET')
        .then(response => {
            console.log('response ======= =to_open =========', response.data, response.status);
            let body = '';
            let orders = response.data;
            if (orders.length === 0) {
                body = 'Apne koi order nahi diya hai.\nAgar koi order dena hai party ko, toh de do';
            }
            else {
                orders.forEach(order => {
                    body += `*_${order.name}_*\n${order.details}\n _Days Passed=>_ *${order.daysPassed}*\n--------------------------------\n`;
                });
            }
            sendWhatsAppMsg(process.env.TO_SUCCESS_PHONE_NUMBERS, body);
        })
        .catch(err => {
            sendWhatsAppMsg(process.env.TO_FAILURE_PHONE_NUMBERS, 'Failed to get data for TO OPEN Orders'+ JSON.stringify(err));
        });
}

function sendWhatsAppNotificationForFromOrders() {
    callFetchApi(`${process.env.GH_ODRS_API_URL}/orders/from_open`, {}, 'GET')
        .then(response => {
            console.log('response ======= =from_open =========', response.data, response.status);
            let body = '';
            let orders = response.data;
            if (orders.length === 0) {
                body = 'Aapke cutomers ke koi bhi order pending nahi hai.\nPlease aur Orders Leke Aao !!';
            }
            else {
                orders.forEach(order => {
                    body += `*_${order.name}_*\n${order.details}\n _Days Passed=>_ *${order.daysPassed}*\n--------------------------------\n`;
                });
            }
            sendWhatsAppMsg(process.env.TO_SUCCESS_PHONE_NUMBERS, body);
        })
        .catch(err => {
            sendWhatsAppMsg(process.env.TO_FAILURE_PHONE_NUMBERS, 'Failed to get data for TO OPEN Orders' + JSON.stringify(err));
        });
}
