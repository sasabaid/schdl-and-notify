const express = require('express');
const router = express.Router();
const AppStatus = require('../models/appStatus');

router.post('/', async (req, res) => {
  const appStatus = new AppStatus(req.body);
  try {
    const savedAppStatus = await appStatus.save();
    res.json(savedAppStatus);
  } catch (err) {
    res.json({ message: err });
  }
});

module.exports = router;
