const express = require('express');
const moment = require('moment-timezone');

const router = express.Router();

router.get('/', (req, res) => {
  const currDate = new Date(moment().format('YYYY-MM-DD HH:mm:ss'));
  res.send(currDate);
});

module.exports = router;
