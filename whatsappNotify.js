require('dotenv/config');
const twilio = require('twilio');
const client = new twilio(process.env.TWILIO_ACCOUNT_ID, process.env.TWILIO_ACCOUNT_SECRET);

const sendWhatsAppMsg = (to_list, body) => {
    let msgIds = [], error = [];
    to_list.split(',').forEach(to => {
        client.messages
        .create({
        from: 'whatsapp:+14155238886',
        body: body,
        to: `whatsapp:+91${to}`
        })
        .then(message => {
            console.log('============message.sid=============', message.sid);
            msgIds.push(message.sid)
            error.push(undefined)
        })
        .catch(err => {
            msgIds.push(undefined)
            error.push(err)
        });
    });
    return { msgIds, error }
}

module.exports=sendWhatsAppMsg;